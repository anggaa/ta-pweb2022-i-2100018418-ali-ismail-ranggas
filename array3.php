<?php
   $nama = array("Angga","Moza","Acha","Nayel","Nadine");

   echo "Menampilkan isi array dengan FOR :<br>";
   for ($i=0; $i <5; $i++)
   {
      echo "$nama[$i]";
      echo "<br />";
   }

   echo "<br>Menampilkan isi array dengan FOREACH :<br>";
   foreach ($nama as $val)
   {
      echo "$val";
      echo "<br />";
   }
?>