<?php
	$hp= array("Merk1"=>"Samsung","Merk2"=>"Nokia","Merk3"=>"Xiaomi");

	echo "Menampilkan isi array asosiatif dengan FOREACH : <br>";
	foreach($hp as $merk=> $nilai){
		echo "$merk=$nilai <br>";
	}

	echo "<br>Menampilkan isi array asosiatif dengan WHILE dan LIST : <br>";
	reset($hp);
	while (list($merk,$nilai)=each($hp)) {
		echo "$merk=$nilai <br>";
	}
?>