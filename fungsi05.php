<?php
function swap(&$arg1, &$arg2){
   echo "inside function before swapping: arg1=$arg1 arg2=$arg2<br>";
   $temp=$arg1;
   $arg1=$arg2;
   $arg2=$temp;
   echo "inside function after swapping: arg1=$arg1 arg2=$arg2<br>";
}
$arg1=10;
$arg2=20;
echo "before calling function : arg1=$arg1 arg2=$arg2<br>";
swap($arg1, $arg2);
echo "after calling function : arg1=$arg1 arg2=$arg2<br>";
?>