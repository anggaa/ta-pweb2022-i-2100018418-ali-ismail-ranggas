<!DOCTYPE html>
<html>
<head>
    <title> Form Validation </title>
    <style>.error {color: #FF0000;}</style>
</head>
<body>

<?php
$namaErr = "";
$nama = "";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if (empty($_POST["nama"])){
        $namaErr = "Nama harus diisi!";
    } else{
        $nama = test_input($_POST["nama"]);
        if (!preg_match("/^[a-zA-Z]*$/", $nama)){
            $namaErr = "Only letters and white space allowed";
        }
    }
}

function test_input($data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

    <h2> Validasi Form </h2>
    <p><span class="error">* Masukkan nama.</span></p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        Nama : <input type="text" name="nama" value="<?php echo $nama;?>">
        <span class="error">* <?php echo $namaErr;?></span>
        <br><br>
        <input type="submit" name="submit" value="Submit">
    </form>

<?php
echo "<h2> Hasil inputan : </h2>";
echo $nama;
echo "<br>"
?>

</body>
</html>