<<?php 
	$hp= array("Merk1"=>"Samsung","Merk2"=>"Nokia","Merk3"=>"Xiaomi");
	echo "<b>Array sebelum diurutkan :<br></b>";
	echo "<pre>";
	print_r($hp);
	echo "</pre>";

	asort($hp);
	reset($hp);
	echo "<b>Array setelah diurutkan dengan asort() :<br></b>";
	echo "<pre>";
	print_r($hp);
	echo "</pre>";

	arsort($hp);
	reset($hp);
	echo "<b>Array setelah diurutkan dengan arsort() :<br></b>";
	echo "<pre>";
	print_r($hp);
	echo "</pre>";
 ?>